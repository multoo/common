<?php

namespace Multoo\Common;

class Header
{
    public static function redirectAndExit($location, $responseCode = 302)
    {
        http_response_code($responseCode);
        header('location: ' . $location);
        exit('location: ' . htmlspecialchars($location, ENT_QUOTES, 'UTF-8'));
    }
}
