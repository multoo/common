<?php

namespace Multoo\Common\Command;

use \Symfony\Component\Console\Style\SymfonyStyle;

class Table
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    protected $lengthByRow = [];

    public function __construct(SymfonyStyle $io)
    {
        $this->io = $io;
    }


    public function printHeaders($rows)
    {
        $headers = [];
        foreach ($rows as $value => $length) {
            if (is_int($length)) {
                $this->lengthByRow[] = $length;
                $headers[] = $value;
            } else {
                $this->lengthByRow[] = 25;
                $headers[] = $length;
            }
        }

        $this->printRow(array_fill(0, count($rows), '-'), '-', '+');
        $this->printRow($headers);
        $this->printRow(array_fill(0, count($rows), '-'), '-', '+');

        return $this;
    }

    public function printRow($row, $padString = ' ', $corner = '|')
    {
        $output = '';

        foreach ($row as $nr => $col) {
            $output .= $padString . substr(str_pad($col, $this->lengthByRow[$nr], $padString), 0, $this->lengthByRow[$nr]) . $padString . $corner;
        }

        $this->io->text($corner . $padString . $output);

        return $this;
    }
}
