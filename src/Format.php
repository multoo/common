<?php

namespace Multoo\Common;

class Format
{
    /**
     * If the postal-code is formatted Dutch, then return it with a space.
     *
     * @param string $str
     * @param boolean $uppercase [optional]
     * @param boolean $withspace [optional]
     * @return string
     */
    public static function postalcode($str, $uppercase = true, $withspace = true)
    {
        $str = trim($str);

        if ($uppercase === true) {
            $str = strtoupper($str);
        } elseif ($uppercase === false) {
            $str = strtolower($str);
        }

        if (preg_match("/^[1-9]{1}[0-9]{3}\s*[A-Z]{2}$/i", $str)) {
            $str = str_replace(" ", "", $str);
            return substr($str, 0, 4) . ($withspace === true ? " " : null) . substr($str, 4, 2);
        } else {
            return $str;
        }
    }

    /**
     * If the IBAN is a dutch-format then return it with spaces.
     *
     * @param string $str
     * @return string
     */
    public static function iban($str)
    {
        $str = trim($str);

        if (!empty($str)) {
            return wordwrap(str_replace(' ', '', $str), 4, ' ', true);
        }

        return $str;
    }
}