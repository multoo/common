<?php

namespace Multoo\Common;

class File
{
    public static function includeIntoVar($filename, $data = "")
    {

        if (is_file($filename)) {
            ob_start();

            if (is_array($data)) {
                extract($data, EXTR_OVERWRITE | EXTR_REFS);
            }

            include $filename;
            $contents = ob_get_contents();
            ob_end_clean();

            return $contents;
        } else {
            throw new \Exception('includeIntoVar file doesn\'t exist: ' . $filename);
        }

        return false;
    }

    public static function fileListing($dir)
    {
        $dirListing = scandir($dir);
        $outputListing = array();
        foreach ($dirListing as $dirListingLine) {
            if (is_file($dir . $dirListingLine)) {
                $outputListing[] = $dirListingLine;
            }
        }

        return $outputListing;
    }

    public static function dirListing($dir)
    {
        $dirListing = scandir($dir);
        $outputListing = array();
        foreach ($dirListing as $dirListingLine) {
            if (is_dir($dir . $dirListingLine) && $dirListingLine != '.' && $dirListingLine != '..') {
                $outputListing[] = $dirListingLine;
            }
        }

        return $outputListing;
    }

    public static function removeExtension($strName)
    {
        $ext = strrchr($strName, '.');

        if ($ext !== false) {
            $strName = substr($strName, 0, -strlen($ext));
        }

        return $strName;
    }
}
