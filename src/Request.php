<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15-02-18
 * Time: 15:33
 */

namespace Multoo\Common;

class Request
{
    /**
     * @param bool $allowUnsafeClientHeaders
     *
     * @return mixed|null
     */
    public static function getIp(bool $allowUnsafeClientHeaders = true)
    {
        if ($allowUnsafeClientHeaders && isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']) && filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ($allowUnsafeClientHeaders && isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) && filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['REMOTE_ADDR']) && filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip ?? null;
    }

    /**
     * @param bool $allowUnsafeClientHeaders
     *
     * @return array
     */
    public static function getIps(bool $allowUnsafeClientHeaders = true)
    {
        if ($allowUnsafeClientHeaders && isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) && filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
            $serverIp = $_SERVER['REMOTE_ADDR'];
            $clientIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['REMOTE_ADDR']) && filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)) {
            $clientIp = $_SERVER['REMOTE_ADDR'];
        }

        return [$clientIp ?? null, $serverIp ?? null];
    }

    /**
     * Checks if the request is an AJAX request.
     *
     * Attention: the check uses $_SERVER['HTTP_X_REQUESTED_WITH'], which is used by many javascript libraries but is no official $_SERVER rule!
     * The check may be not 100% correct.
     *
     * @return boolean
     */
    public static function ajax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ? true : false;
    }

    /**
     * Checks if the request is an CLI request.
     *
     * @return boolean
     */
    public static function cli()
    {
        return php_sapi_name() == 'cli' ? true : false;
    }

    /**
     * Checks if any content-type headers are set. A content-type header can mean a PDF download..
     *
     * @return string
     */
    public static function contentType()
    {
        $return = null;
        $headers = headers_list();

        foreach ($headers as $header) {
            $header = explode(":", $header);

            if (strcasecmp(trim($header[0]), 'Content-type') === 0) {
                $return = $header[1];
                break;
            }
        }

        return $return;
    }
}
