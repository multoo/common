<?php

namespace Multoo\Common;

/**
 * Helper class to deal with Ajax requests.
 */
class Ajax
{

    /**
     * Stops request when detects Ajax.
     *
     * @return boolean
     */
    public static function deny()
    {
        if (Request::ajax() === true) {
            http_response_code(403);
            exit('AJAX request denied');
        }

        return true;
    }

    /**
     * Stops request when detects anything but Ajax, only allows Ajax requests.
     *
     * @return boolean
     */
    public static function only()
    {
        if (Request::ajax() === false) {
            http_response_code(403);
            exit('Only AJAX request allowed');
        }

        return true;
    }
}
