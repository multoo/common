<?php

namespace Multoo\Common;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Csrf
{
    /**
     * @var SessionInterface
     */
    protected static $session;

    /**
     * @var bool
     */
    protected static $triggered = false;

    public static function getToken()
    {
        if (self::$session === null) {
            throw new \Exception('SESSION NOT SET');
        }

        if (!self::$session->has('csrf_token')) {
            $token = md5(uniqid(microtime(), true));
            self::$session->set('csrf_token', $token);
        }

        return self::$session->get('csrf_token');
    }

    //TODO: Make Request class required and remove $_POST when projects no longer use $_POST
    public static function verifyFormToken(\Symfony\Component\HttpFoundation\Request $request = null)
    {
        $ajaxRequest = \Multoo\Common\Request::ajax();

        $token = self::getToken();

        if ($ajaxRequest === false && empty($token)) {
            return false;
        } elseif ($ajaxRequest === false && !isset($_POST["csrf_token"])) {
            return false;
        } elseif ($ajaxRequest === false && $token !== $_POST["csrf_token"]) {
            return false;
        } else {
            unset($_POST["csrf_token"]);
            $request !== null && $request->request->remove('csrf_token');
        }

        return true;
    }

    public static function makeFormsCsrfSafe($content)
    {
        $pattern = "/(<\/form>)/i";

        return preg_replace($pattern, "<input type=\"hidden\" name=\"csrf_token\" value=\"" . self::getToken() . "\" />\n$1", $content);
    }

    //TODO: Make Request class required and remove $_POST when projects no longer use $_POST
    public static function makeItCsrfSafe(\Symfony\Component\HttpFoundation\Request $request = null)
    {
        self::getToken();

        if (isset($_POST) && !empty($_POST) && !self::verifyFormToken($request)) {
            $GLOBALS['_UNSAFE_POST'] = $_POST;
            unset($_POST);
            $_POST = [];
            $request !== null && $request->request->replace([]);

            self::$triggered = true;

            return false;
        }

        return true;
    }

    public static function setSession(SessionInterface $session)
    {
        self::$session = $session;
    }
}
