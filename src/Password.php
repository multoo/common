<?php

namespace Multoo\Common;

/**
 * Class with password helpers
 *
 * @author alex
 */
class Password
{

    /**
     * Create a random password
     *
     * @param int $length The length of the password
     * @param string $availableSets 'Lud' is normal, 'Slud' is strong. 'l' => lowercase letters, 'u' => uppercase latters, 'd' => numbers, 's' => special chars
     * @return string
     */
    public static function generate($length = 9, $availableSets = 'lud')
    {
        $sets = array();
        if (strpos($availableSets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($availableSets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($availableSets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($availableSets, 's') !== false) {
            $sets[] = '!@#%?';
        }

        $all = '';
        $password = '';

        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);

        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);

        return $password;
    }

    /**
     * Check strength of the password
     *
     * @param string $password Password to check
     * @return boolean
     */
    public static function isStrong($password)
    {
        $return = false;
        if (preg_match("/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $password)) {
            $return = true;
        }

        return $return;
    }
}
